---
layout: default
---
<picture class="full pixels">
    <source srcset="assets/splash-dark.png" media="(prefers-color-scheme: dark)">
    <img src="assets/splash.png">
</picture>

The [Freedesktop SDK project](https://gitlab.com/freedesktop-sdk/freedesktop-sdk) is a free and open source project that provides Platform and SDK runtimes for [Flatpak](https://flatpak.org) apps and runtimes based on Freedesktop modules. It was originally started as a Flatpak subproject to create a basic-neutral runtime. It is now a separate project, similar to what happened with the GNOME and KDE runtimes..

Along side Flatpak applications we are exploring how to use runtime in other ways, such as providing a minimal bootable system for anyone to use in their own project and bootable VM images which will use the freedesktop-sdk as a base.

This is a diagram of how the Freedeskop SDK runtimes interact with the rest of the Flatpak ecosystem

<picture class="full">
	<source srcset="assets/fdosdk-dia.png" media="(prefers-reduced-motion: reduce)">
	<img src="assets/fdosdk-dia.gif">
</picture>


The Freedesktop SDK project is not an endorsement of a particular platform or selection of technologies by the Freedesktop organization. The SDK aims to support a common interoperable platform for projects such as GNOME, KDE and Flatpak by providing integration and validation of a shared set of dependencies.

## Events & Talks

* [Unveiling the latest edition of Freedesktop SDK, v18.08](https://www.youtube.com/watch?v=rhEzTQg_LEk), GUADEC Almeria, Spain, 2018
* [Freedesktop-SDK Status Update and Future Plans](https://www.youtube.com/watch?v=ky-inEefWpE), GUADEC Thessaloniki, Greece, 2019
* [Distributing Freedesktop SDK applications to Flatpak, Snapd and Docker](https://www.youtube.com/watch?v=IDetaPf7dLs), All Systems Go!, Berlin, Germany, 2019

## Sponsors

We would like to thank our generous sponsors both for their time and resources to help build *Freedesktop SDK*.

If you would like to help us with sponsorship please contact us on [Matrix](https://matrix.to/#/#freedesktop-sdk:matrix.org).

<div class="pixelgrid">
    <a href="https://www.codethink.co.uk/"><picture class="full">
        <source srcset="assets/sponsor/codethink-l.png" media="(prefers-color-scheme: dark)">
        <img src="assets/sponsor/codethink.png" alt="Codethink">
    </picture></a>
    <a href="https://www.arm.com/markets/computing-infrastructure/works-on-arm"><picture class="full">
        <source srcset="assets/sponsor/worksonarm-l.png" media="(prefers-color-scheme: dark)">
        <img src="assets/sponsor/worksonarm.png" alt="Works on ARM">
    </picture></a>
    <a href="https://osuosl.org/"><picture class="full">
        <source srcset="assets/sponsor/osulab-l.png" media="(prefers-color-scheme: dark)">
        <img src="assets/sponsor/osulab.png" alt="OSU Lab">
    </picture></a>
</div>

## Get Involved
We are a group of people who are passionate about the Linux application ecosystem, we work together to deliver a runtime that benefits everyone, if you are passionate and want to get involved please do, if you need help using us, you can also contact us for help! 

Any contribution however large or small will be greatly appreciated, documentation/bug reports/feature requests all help! 

  * Join us on Matrix at [#freedesktop-sdk:matrix.org](https://matrix.to/#/#freedesktop-sdk:matrix.org).
  * Issues are tracked on [FDO Gitlab](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues).
  * We have a [mailing list](mailto:freedesktop-sdk@lists.freedesktop.org) for easy communication across time zones.
  
